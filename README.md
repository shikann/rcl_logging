# rcl_logging

#### 介绍

rcl_logging节点ROS2的LOG日志记录实现。主要有以下三种方式实现: spdlog, log4cxx noop
- rcl_logging_spdlog
[rcl_logging_spdlog](include/rcl_logging_spdlog/logging_interface.h) 参考:
 - 初始化
 - 记录消息
 - 设置等级
 - 关闭
一些实用的工具参考:
    - [rcl_logging_spdlog/visibility_control.h](include/rcl_logging_spdlog/visibility_control.h)
- rcl_logging_log4cxx
- rcl_logging_noop

#### 软件架构
软件架构说明

https://github.com/ros2/rcl_logging.git

文件内容:

```
./
├── CONTRIBUTING.md
├── LICENSE
├── README.md
├── rcl_logging_log4cxx
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── Doxyfile
│   ├── cmake
│   │   └── FindLog4cxx.cmake
│   ├── include
│   │   └── rcl_logging_log4cxx
│   ├── package.xml
│   └── src
│       └── rcl_logging_log4cxx
├── rcl_logging_noop
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── Doxyfile
│   ├── package.xml
│   └── src
│       └── rcl_logging_noop
└── rcl_logging_spdlog
    ├── CHANGELOG.rst
    ├── CMakeLists.txt
    ├── Doxyfile
    ├── QUALITY_DECLARATION.md
    ├── README.md
    ├── include
    │   └── rcl_logging_spdlog
    ├── package.xml
    ├── src
    │   └── rcl_logging_spdlog.cpp
    └── test
        ├── benchmark
        ├── fixtures.hpp
        └── test_logging_interface.cpp
```

#### 安装教程

1. 下载rpm包

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rcl_logging/ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rcl_logging/ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.x86_64.rpm
```

2. 安装rpm包

aarch64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.x86_64.rpm
```

#### 使用说明

依赖环境安装:

```
sh /opt/ros/foxy/install_dependence.sh
```

安装完成以后，在/opt/ros/foxy/目录下如下输出,则表示安装成功。

输出:

```
rcl_logging_log4cxx
├── include
│   └── rcl_logging_log4cxx
│       ├── logging_interface.h
│       └── visibility_control.h
├── lib
│   └── librcl_logging_log4cxx.so
└── share
    ├── ament_index
    │   └── resource_index
    ├── colcon-core
    │   └── packages
    └── rcl_logging_log4cxx
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh
rcl_logging_noop
├── lib
│   └── librcl_logging_noop.so
└── share
    ├── ament_index
    │   └── resource_index
    ├── colcon-core
    │   └── packages
    └── rcl_logging_noop
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh
rcl_logging_spdlog
├── include
│   └── rcl_logging_spdlog
│       ├── logging_interface.h
│       └── visibility_control.h
├── lib
│   └── librcl_logging_spdlog.so
└── share
    ├── ament_index
    │   └── resource_index
    ├── colcon-core
    │   └── packages
    └── rcl_logging_spdlog
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh

```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
