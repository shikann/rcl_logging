# rcl_logging

#### Description
The rcl_logging node 

Logging implementations for ROS 2

- rcl_logging_spdlog
Package supporting an implementation of logging functionality using `spdlog`.

[rcl_logging_spdlog](include/rcl_logging_spdlog/logging_interface.h) logging interface implementation can:
 - initialize
 - log a message
 - set the logger level
 - shutdown

Some useful internal abstractions and utilities:
  - Macros for controlling symbol visibility on the library
    - [rcl_logging_spdlog/visibility_control.h](include/rcl_logging_spdlog/visibility_control.h)
- rcl_logging_log4cxx
- rcl_logging_noop

#### Software Architecture
Software architecture description

https://github.com/ros2/rcl_logging.git

input:

```
./
├── CONTRIBUTING.md
├── LICENSE
├── README.md
├── rcl_logging_log4cxx
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── Doxyfile
│   ├── cmake
│   │   └── FindLog4cxx.cmake
│   ├── include
│   │   └── rcl_logging_log4cxx
│   ├── package.xml
│   └── src
│       └── rcl_logging_log4cxx
├── rcl_logging_noop
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── Doxyfile
│   ├── package.xml
│   └── src
│       └── rcl_logging_noop
└── rcl_logging_spdlog
    ├── CHANGELOG.rst
    ├── CMakeLists.txt
    ├── Doxyfile
    ├── QUALITY_DECLARATION.md
    ├── README.md
    ├── include
    │   └── rcl_logging_spdlog
    ├── package.xml
    ├── src
    │   └── rcl_logging_spdlog.cpp
    └── test
        ├── benchmark
        ├── fixtures.hpp
        └── test_logging_interface.cpp
```

#### Installation

1.  Download RPM

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rcl_logging/ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rcl_logging/ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.x86_64.rpm
```

2.  Install RPM

aarch64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rcl_logging-1.1.0-2.oe2203.x86_64.rpm
```

#### Instructions

Dependence installation:

```
sh /opt/ros/foxy/install_dependence.sh
```

Exit the following output file under the /opt/ros/foxy/ directory,Prove that the software installation is successful.

output:
```
rcl_logging_log4cxx
├── include
│   └── rcl_logging_log4cxx
│       ├── logging_interface.h
│       └── visibility_control.h
├── lib
│   └── librcl_logging_log4cxx.so
└── share
    ├── ament_index
    │   └── resource_index
    ├── colcon-core
    │   └── packages
    └── rcl_logging_log4cxx
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh
rcl_logging_noop
├── lib
│   └── librcl_logging_noop.so
└── share
    ├── ament_index
    │   └── resource_index
    ├── colcon-core
    │   └── packages
    └── rcl_logging_noop
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh
rcl_logging_spdlog
├── include
│   └── rcl_logging_spdlog
│       ├── logging_interface.h
│       └── visibility_control.h
├── lib
│   └── librcl_logging_spdlog.so
└── share
    ├── ament_index
    │   └── resource_index
    ├── colcon-core
    │   └── packages
    └── rcl_logging_spdlog
        ├── cmake
        ├── environment
        ├── hook
        ├── local_setup.bash
        ├── local_setup.dsv
        ├── local_setup.sh
        ├── local_setup.zsh
        ├── package.bash
        ├── package.dsv
        ├── package.ps1
        ├── package.sh
        ├── package.xml
        └── package.zsh

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
